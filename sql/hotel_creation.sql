CREATE SCHEMA Hotel;
SET search_path TO Hotel, public;
CREATE TABLE Hotel.Client (
	id_client serial NOT NULL,
	nom_client text NOT NULL,
	prenom_client text NOT NULL,
	mail text NOT NULL,
	--- clefs candidates
	PRIMARY KEY (id_client)
);
CREATE TABLE Hotel.Chambre (
	id_chambre serial NOT NULL,
	tarif_chambre integer NOT NULL CHECK (tarif_chambre >= 0),
	--- clefs candidates
	PRIMARY KEY (id_chambre)
);
CREATE TABLE Hotel.Boisson (
	boisson text NOT NULL,
  	prix integer NOT NULL CHECK (prix > 0),
  	-- clefs candidates
  	PRIMARY KEY (boisson)
);
CREATE TABLE Hotel.Consommation (
	id_chambre serial NOT NULL,
	boisson text NOT NULL,
	nbre_conso integer NOT NULL,
	--- clefs etrangeres
	FOREIGN KEY (id_chambre) REFERENCES Hotel.Chambre(id_chambre),
	FOREIGN KEY (boisson) REFERENCES Hotel.Boisson(boisson),
	PRIMARY KEY (id_chambre),
	--- contraintes elementaires
	CHECK (id_chambre = id_chambre)
);
CREATE TABLE Hotel.Reservation (
	id_facture serial NOT NULL,
	id_chambre serial NOT NULL,
	id_client serial NOT NULL,
	date_debut date NOT NULL,
	date_fin date NOT NULL,
	date_paiement date,
	--- clefs candidates
	PRIMARY KEY (id_facture),
	UNIQUE(id_chambre, date_debut),
	UNIQUE (id_chambre, date_fin),
	--- clefs etrangeres
	FOREIGN KEY (id_chambre) REFERENCES Hotel.Chambre(id_chambre),
	FOREIGN KEY (id_client) REFERENCES Hotel.Client(id_client),
	--- contraintes elementaires
	CHECK (date_debut < date_fin)
);
