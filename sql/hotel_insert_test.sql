INSERT INTO Hotel.Client
	VALUES (default, 'Durand', 'Jean', 'jean.durand@mail.fr');
INSERT INTO Hotel.Client
	VALUES (default, 'Dupont', 'Jacques', 'jacques.dupont@mail.fr');
INSERT INTO Hotel.Client
	VALUES (default, 'Martin', 'Martine', 'martine.martin@mail.fr');

INSERT INTO Hotel.Chambre
	VALUES (1, 80);
INSERT INTO Hotel.Chambre
	VALUES (2, 120);
INSERT INTO Hotel.Chambre
	VALUES (3, 100);
INSERT INTO Hotel.Chambre
	VALUES (4, 150);
	
INSERT INTO Hotel.Reservation
	VALUES (default, 2, 3, '2021-04-10', '2021-04-15');
INSERT INTO Hotel.Reservation
	VALUES (default, 3, 1, '2021-04-10', '2021-04-15');
INSERT INTO Hotel.Reservation
	VALUES (default, 1, 1, '2021-04-16', '2021-04-21');
INSERT INTO Hotel.Reservation
	VALUES (default, 2, 2, '2021-04-17', '2021-04-25');
--- Test insertion violant une contrainte
--- INSERT INTO Hotel.Reservation
---	VALUES (default, 3, 2, '2020-12-17', '2020-12-12');

INSERT INTO Hotel.Boisson
  VALUES('Biere',3);
INSERT INTO Hotel.Boisson
  VALUES('Soda',2);
INSERT INTO Hotel.Boisson
  VALUES('Rhum',5);

INSERT INTO Hotel.Consommation
	VALUES (2, 'Biere', 3);
INSERT INTO Hotel.Consommation
	VALUES (1, 'Soda', 1);
--- Test insertion violant une contrainte
--- INSERT INTO Hotel.Consommation
---	VALUES (10, 'Biere', 1);
