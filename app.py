import time
import datetime
from datetime import *
from flask import *
from flask import request, redirect
import sys
import psycopg2
import psycopg2.extras
import json
import logging
import locale
import pymongo

locale.setlocale(locale.LC_TIME,'')

# from flask import Flask, flash, redirect, render_template, session, request, url_for
# from jinja2 import Template

app = Flask(__name__)
app.secret_key = 'some_secret'
affich = ''

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
logging.debug("Debug mode activated")

""" Page d'accueil du site """
@app.route('/')
def index():
    session = {}
    session['dateDuJour'] = date.today().isoformat()
    flash('Bienvenue sur le site de l\'Hôtel TrucMuche')
    flash('Nous sommes le : ' + session['dateDuJour'])
    return render_template('accueil.html', session=session)

# Page description des chambres
@app.route('/chambres')
def chambres():
    result = pgsql_select('SELECT * FROM hotel.chambre ORDER BY tarif_chambre', [])
    return render_template('chambres.html', result=result)

# Page de reservation
@app.route('/reservation', methods=['POST', 'GET'])
def reservation():
    if request.method == 'POST':
        date_debut = request.form['date_debut']
        date_fin = request.form['date_fin']
        sql = """SELECT id_chambre, tarif_chambre FROM hotel.chambre
        WHERE id_chambre NOT IN (
            SELECT id_chambre FROM hotel.reservation WHERE (
                date_debut
                BETWEEN
                '""" + date_debut + """'::date
                AND
                '""" + date_fin   + """'::date
                OR
                date_fin
                BETWEEN
                '""" + date_debut + """'::date
                AND
                '""" + date_fin + """'::date
            )
        )
        ORDER BY id_chambre;"""
        if date_debut >= date_fin:
            flash("Erreur: la date de d\'arrivée doit être antérieure à la date de départ")
            return render_template("resa.html")
        elif date_debut <= date.today().isoformat():
            flash("La date d\'arrivée doit être avant la date du jour")
            return render_template("resa.html")
        else:
            chDispo = pgsql_select(sql, [])
            # logging.debug(chDispo)
            if chDispo:
                return render_template('resa.html', chDispo = chDispo)
    return render_template('resa.html', session=session)

# Page utilisateur
@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        mail = pgsql_display_client(request.form.get('email'))
        # logging.debug("Message %s" % (mail))
        if mail:
            for row in mail:
                session['nom'] = row[1]
                session['prenom'] = row[2]
                session['mail'] = row[3]
                # logging.debug("/login -> authentication : " + session['nom'])
                flash('Bonjour, vous êtes authentifié en tant que  ' + session['prenom'] + ' ' + session['nom'])
                flash(' Votre adresse de contact est : ' + session['mail'])
        else:
            flash('Utilisateur inconnu')
    return render_template('login.html', session=session)

@app.route('/signup', methods=['Post', 'GET'])
def signup():
    if request.method == 'POST':
        dp = pgsql_add_client(request.form['nom'],request.form['prenom'],request.form['mail'])
        req = request.form
        query = """INSERT INTO hotel.client VALUES (DEFAULT, '%s', '%s','%s');"""
        session['nom'] = request.form['nom']
        session['prenom'] = request.form['prenom']
        session['mail'] = request.form['mail']
        # logging.debug('Nom : ' + session['nom'] + ' Prenom : '+ session['prenom'] + ' Courriel : '+ session['mail'])
        # logging.debug(session)

        return redirect(request.url)

    return render_template('signup.html', session=session)

@app.route('/logout')
def logout():
    session.pop('nom', None)
    session.pop('prenom', None)
    session.pop('mail', None)
    return redirect(url_for('login'))

# Page d'administration
@app.route('/admin', methods=['POST', 'GET'])
def admin():
    if request.method == 'POST':
        if request.form['action'] == 'create':
            pgsql_create_db()
            print("Base de donnée créée")
            pgsql_disconnect()
        elif request.form['action'] == 'insert':
            pgsql_insert_test()
            print("Données de tests OK")
            pgsql_disconnect()
        elif request.form['action'] == 'drop':
            pgsql_drop_db()
            print("Base de donnée vidée")
            pgsql_disconnect()
        elif request.form['action'] == 'clients':
            try:
                result = pgsql_liste_client()
                pgsql_disconnect()
                # logging.debug(result)
                return render_template('admin.html', result = result)
            except Exception as e :
                flash('Désolé, une erreur s\'est produite.')
                return redirect(url_for('admin', error=str(e)))
        else:
            return render_template("admin.html")
    return render_template('admin.html')

### Fonctions pour les Base de données ###

# SGBD Postgresql
def pgsql_connect():
    # Try connection
    db = getattr(g, 'db_pgsql', None)
    if db is None:
        try:
            # g.db_pgsql = psycopg2.connect("host=pgsql dbname=cymillet user=cymillet password=l4,;nux")
            g.db_pgsql = psycopg2.connect("host=localhost dbname=hotel user=cyril password=linux")
            print('Connexion OK')
            return g.db_pgsql
        except Exception as e:
            flash('Désolé, connection à la base de donnée indisponible.')
            return redirect(url_for('index', error=str(e)))
    else:
        return db

def pgsql_disconnect():
    # Try to disconnect
    db = getattr(g, 'db_pgsql', None)
    if db is not None:
        g.db_pgsql.close()
        g.db_pgsql = None

def pgsql_drop_db():
    db = pgsql_connect()
    cursor = db.cursor()
    try:
        with app.open_resource('sql/hotel_destruction.sql') as f:
            cursor.execute(f.read().decode('utf_8'))
        db.commit()
        flash('La base donnée est détruite')
    except Exception as e:
        flash('Désolé, une erreur s\'est produite')
        flash(str(e))
        return redirect(url_for('admin', error=str(e)))

def pgsql_create_db():
    db = pgsql_connect()
    cursor = db.cursor()
    try:
        with app.open_resource('sql/hotel_creation.sql') as f:
            cursor.execute(f.read().decode('utf_8'))
        db.commit()
        flash('La base de donnée est créée')
    except Exception as e:
        flash('Désolé, une erreur s\'est produite')
        flash(str(e))
        return redirect(url_for('admin', error=str(e)))

def pgsql_select(command, param):
    #flash(command)
    db = pgsql_connect()
    # pour récupérer les attibuts des relations
    cursor = db.cursor()
    try:
        cursor.execute(command, param)
        rows = cursor.fetchall()
        cursor.close()
        return rows
    except Exception as e :
        flash('Désolé, une erreur s\'est produite.')
        return redirect(url_for('admin', error=str(e)))

def pgsql_insert_test():
    db = pgsql_connect()
    cursor = db.cursor()
    try:
        with app.open_resource('sql/hotel_insert_test.sql') as f:
            cursor.execute(f.read().decode('utf-8'))
        db.commit()
        flash('Les données de test ont été importées avec succès')
    except Exception as e:
        flash('Désolé, une erreur s\'est produite')
        flash(str(e))
        return redirect(url_for('admin', error=str(e)))

def pgsql_insert(command, param):
    #flash(command)
    db = pgsql_connect()
    cursor = db.cursor()
    try:
        cursor.execute(command, param)
        nb = cursor.rowcount
        cursor.close()
        db.commit()
        return db
    except Exception as e:
        flash('Désolé, une erreur s\'est produite.')
        return redirect(url_for('signup', error=str(e)))

def pgsql_liste_client():
    return pgsql_select('SELECT nom_client, prenom_client, mail FROM hotel.client ORDER BY nom_client',[])

def pgsql_liste_mail():
    return pgsql_select('SELECT mail FROM hotel.client', [])

def pgsql_liste_chambre():
    return pgsql_select('SELECT id_chambre FROM hotel.chambre EXCEPT SELECT id_chambre::integer FROM hotel.reservation WHERE date_debut < (%s) and date_fin > (%s);', [fin,debut])

def pgsql_liste_chambre_occupee(jour):
    return pgsql_select('SELECT id_chambre FROM hotel.reservation where date_debut <= and date_fin > (%s);', [jour,jour])

def pgsql_display_client(mail):
    return pgsql_select('select * from hotel.client where mail = (%s);', [mail])

def pgsql_add_client(nom,prenom,mail):
    return pgsql_insert('insert into hotel.client values(DEFAULT, (%s), (%s), (%s));', [nom, prenom, mail])

# Mongodb

def get_mg_db():
    # db = MongoClient("mongodb://mongodb.emi.u-bordeaux.fr:27017").cymillet
    db = MongoClient("mongodb://localhost:27017")
    return db

def mgdb_drop_db():
    mgdb = get_mg_db()
    mgdb.chambres.drop()
    mgdb.comments.drop()

def mgdb_init_db():
    mgdb = get_mg_db()
    with app.open_resource('./sql/desc_ch.json') as f:
        mgdb.chambres.insert(json.loads(f.read().decode('utf_8')))

def mgdb_display_chambre(idChambre):
    mgdb = get_mg_db()
    if mgdb:
        return mgdb.chambres.find({"chambre_id":int(idChambre)})
    else:
        return none

def mgdb_display_comments(idChambre):
    mgdb = get_mg_db()
    if mgdb:
        return mgdb.chambres.find({"chambre_id":int(idChambre)})
    else:
        return none

if __name__ == '__main__':
    app.run(debug = True)
